public class pocetna {

    public static void main(String[] args) {


        System.out.println(indeksi("Marko",1,3));

        System.out.println(izmena("{surname}","Mijailovic"));
    }

    public static Integer stringToInt(String s){
        return Integer.parseInt (s) ;
    }

    public static String sastavi(String a, String b){
        return a + b;
    }

    public static String obrni(String s){
        StringBuilder sb = new StringBuilder(s);
        sb.reverse();
        return sb.toString();

    }

    public static String indeksi(String a, int b, int c) {
        String s = a.substring(b,c);
        return s;
    }

    public static String izmena(String a, String b){
        String s = "Ivan {surname} je kralj";
        s = s.replace(a,b);
        return s;
    }
}
